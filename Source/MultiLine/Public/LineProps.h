// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

struct LineProps
{
	LineProps(){}
	LineProps(TArray<FVector> & p, FLinearColor color, float w) : points(p), linear_color(color), width(w)
	{}

	TArray<FVector> points;
	FLinearColor linear_color;
	float width;
};
