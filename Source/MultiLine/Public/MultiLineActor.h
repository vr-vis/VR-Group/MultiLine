// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "LineProps.h"
#include "MultiLineActor.generated.h"

UCLASS()
class MULTILINE_API AMultiLineActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMultiLineActor();

	void CreateMesh(TArray<LineProps> & line_props_vec, bool has_collision_context = false);

	void UpdateMesh(TArray<LineProps> & line_props_vec);

	UFUNCTION(BlueprintCallable, Category = "SunShine")
		void SetFlatShading(bool use_flat_shading);

	virtual void Destroyed() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void CreateVertices(LineProps & line_props, FOccluderVertexArray & vertices);
	void SetUpTriangles(size_t index_offset, TArray<int32> & triangles);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh_;

	UPROPERTY(VisibleAnywhere)
		UMaterial* material_;
};
